package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CadastrarEstagiarioPage {

	private WebDriver driver;
	
	public CadastrarEstagiarioPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	public void selecionarCombo(String xpath, String valor) {
		WebElement element = driver.findElement(By.xpath(xpath));
		Select combo = new Select(element);
		combo.selectByVisibleText(valor);
	}
	public void acessaCadastroEstagiarios() {
		driver.findElement(By.xpath("/html/body/div/ul/li[4]/a/span")).click();
		driver.findElement(By.xpath("/html/body/div/ul/li[4]/div/div/a[1]")).click();
	}
	public void setConcedente(String valor ) {
		selecionarCombo("/html/body/div/div/div/div[2]/form/div/div[2]/div[1]/div/div/select", valor);
	}
	public void setCurso(String valor) {
		selecionarCombo("/html/body/div/div/div/div[2]/form/div/div[2]/div[2]/div/div/select", valor);
	}
	public void setDisciplina(String valor) {
		selecionarCombo("/html/body/div/div/div/div[2]/form/div/div[2]/div[3]/div/div/select", valor);
	}
	public void setTurno(String valor) {
		selecionarCombo("/html/body/div/div/div/div[2]/form/div/div[3]/div[1]/div/div/select", valor);
	}
	public void setQuantidadeAlunos(String quantidade) {
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/form/div/div[3]/div[2]/div/div/input")).sendKeys(quantidade);
	}
	public void setCustoAluno(String custo) {
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/form/div/div[3]/div[3]/div/div/input")).sendKeys(custo);
	}
	public void setPreceptor(String valor) {
		selecionarCombo("/html/body/div/div/div/div[2]/form/div/div[3]/div[4]/div/div/select", valor);
	}
	public void setlocal(String valor) {
		selecionarCombo("/html/body/div/div/div/div[2]/form/div/div[4]/div/div/div/select", valor);
	}
	public void setEstabelecimento(String valor) {
		selecionarCombo("/html/body/div/div/div/div[2]/form/div/div[5]/div[1]/div/div/select", valor);
	}
	public void setSetor(String setor) {
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/form/div/div[5]/div[2]/div/div/input")).sendKeys(setor);
	}
	public void setData(String data) {
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/form/div/div[6]/div/div/div/div/input")).sendKeys(data);
	}
	public void concluirCadastro() {
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/form/button")).click();
	}
}