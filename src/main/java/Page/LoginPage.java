package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	private WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		super();
		this.driver = driver;			
	}
	public void visita() {
		driver.get("https://contra-partida.herokuapp.com/");
	}
	public void acessar(String usuario, String senha) {
		driver.findElement(By.name("username")).sendKeys(usuario);
		driver.findElement(By.name("password")).sendKeys(senha);
		driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[2]/button")).click();
	}
}
