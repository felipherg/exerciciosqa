import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import Page.CadastrarEstagiarioPage;
import Page.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;

public class CadastrarEstagiario {

	private ChromeDriver driver;
	private LoginPage login; 
	private CadastrarEstagiarioPage cadastrar;
	
	@Before
	public void inicializaLogar() {
		WebDriverManager.chromedriver().setup();
		this.driver = new ChromeDriver();
		this.login = new LoginPage(driver);
		this.cadastrar = new CadastrarEstagiarioPage(driver);
		
		login.visita();
		login.acessar("", "");
	}
	@Test
	public void cadastroEstagio() {
		cadastrar.acessaCadastroEstagiarios();
		cadastrar.setConcedente("Ortotrauma");
		cadastrar.setCurso("Medicina");
		cadastrar.setDisciplina("Pediatria");
		cadastrar.setTurno("Integral");
		cadastrar.setQuantidadeAlunos("2");
		cadastrar.setCustoAluno("60");
		cadastrar.setPreceptor("Wallace");
		cadastrar.setlocal("Hospital de Trauma");
		cadastrar.setEstabelecimento("Unidade de sa�de");
		cadastrar.setSetor("Setor");
		cadastrar.setData("05/10/2020");
	}
	@After
	public void enviar() {
	/*cadastrar.concluirCadastro();
	driver.close();*/
	}	
}
